package ch.cern.sparkinparallel

import ch.cern.admon.core.utils.DotEnv
import org.apache.spark.sql.SparkSession
import org.apache.spark.SparkConf

object Main {
  def main(args: Array[String]): Unit = {
    val spark = SparkSession.builder.appName("SparkInParallel").getOrCreate()
    val df = spark.read.format("parquet").load("hdfs://analytix/project/monitoring/collectd/cpu/2022/03/01")

    println(df.count())
    df.show()
  }
}


