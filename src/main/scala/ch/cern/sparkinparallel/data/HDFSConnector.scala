import org.apache.spark.sql.{DataFrame, SparkSession}


object HDFSConnector {
  val HDFS_COLLECTD = "hdfs://analytix/project/monitoring/collectd/cpu/2022/03/01"
  val HDFS_COLLECTD_SCALED = "hdfs://analytix/project/monitoring/collectd/cpu/2022/03"

  def loadCPU(spark: SparkSession): DataFrame = {
    spark.read.format("parquet").load(HDFS_COLLECTD)
  }
}
