package ch.cern.sparkinparallel

import ch.cern.admon.core.model.{DFColumn, InputDataConfig, SourceFields}
import ch.cern.admon.core.processing.InputConfigProcess.AggregateToPayload
import ch.cern.sparkinparallel.scenario.TestTask
import org.apache.spark.sql.functions.{col, from_json}
import org.apache.spark.sql.types.{DoubleType, LongType, StringType, StructField, StructType}
import org.apache.spark.sql.{DataFrame, SparkSession}


object StreamJobs {
    def process(spark: SparkSession, kafkaPassword: String, tasks: List[TestTask]): Unit = {
        for (task <- tasks) {
            var df = spark.readStream.format("kafka")
                .option("kafka.ssl.truststore.location", "resources/truststore.jks")
                .option("kafka.ssl.truststore.password", kafkaPassword)
                .option("kafka.ssl.keystore.location", "resources/keystore.jks")
                .option("kafka.ssl.keystore.password", kafkaPassword)
                .option("kafka.ssl.key.password", kafkaPassword)
                .option("kafka.security.protocol", "SASL_SSL")
                .option("kafka.sasl.mechanism", "GSSAPI")
                .option("kafka.sasl.kerberos.service.name", "kafka")
                .option("kafka.bootstrap.servers", "monit-kafkay-eed7c3fb30.cern.ch:9093,monit-kafkay-573f3ef962.cern.ch:9093,monit-kafkay-f393cbf994.cern.ch:9093,monit-kafkay-b6f6cf237c.cern.ch:9093,monit-kafkay-ce8cd4fdc8.cern.ch:9093,monit-kafkay-961c15519d.cern.ch:9093,monit-kafkay-1417749746.cern.ch:9093,monit-kafkay-d18cbd24f0.cern.ch:9093,monit-kafkay-ecfe28a060.cern.ch:9093,monit-kafkay-67acc26a80.cern.ch:9093,monit-kafkay-a29ccd22b4.cern.ch:9093,monit-kafkay-4d50c2f06a.cern.ch:9093,monit-kafkay-3f82791ad6.cern.ch:9093,monit-kafkay-d98673aa25.cern.ch:9093,monit-kafkay-b0e83e2ebc.cern.ch:9093,monit-kafkay-59d76883de.cern.ch:9093,monit-kafkay-f4c6e94a2e.cern.ch:9093,monit-kafkay-7c06bd3b6f.cern.ch:9093,monit-kafkay-1182c933d6.cern.ch:9093,monit-kafkay-e2f2dc45d2.cern.ch:9093,monit-kafkay-139aed5efb.cern.ch:9093,monit-kafkay-c0808ac86b.cern.ch:9093,monit-kafkay-ebd96dbca7.cern.ch:9093,monit-kafkay-90b46a08de.cern.ch:9093,monit-kafkay-ee4d41b68b.cern.ch:9093,monit-kafkay-7385046b84.cern.ch:9093,monit-kafkay-fe65ff1d80.cern.ch:9093,monit-kafkay-86c5d3d4ab.cern.ch:9093,monit-kafkay-844e341485.cern.ch:9093,monit-kafkay-0da67498aa.cern.ch:9093")
                .option("subscribe", topic(task))
                .option("startingTimestamp", (task.startTask * 1000).toString)
                .option("endingTimestamp", (task.endTask * 1000).toString)
                .load()
            df = df.select(col(DFColumn.JSONdata.toString).cast("string"))
            df = withSchema(df)
            // df = processStream(df, task.inputDataConfig)

            df.writeStream.format("console")
                .outputMode("append")
                .start()
                .awaitTermination()
        }
    }

    def topic(task: TestTask): String = {
        val SourceFields(producer, typePrefix, topicType) = task.inputDataConfig.sourceConfigs.head.sourceFields
        s"${producer}_${typePrefix}_$topicType"
    }

    def processStream(dataFrame: DataFrame, inputDataConfig: InputDataConfig): DataFrame = {
        val transform = inputDataConfig.sourceConfigs.head.transformConfig
        // filter
        var workDF = if (transform.filterExpression.isDefined) {
            dataFrame.where(transform.filterExpression.get)
        } else {
            dataFrame
        }
        // select
        workDF = if (transform.selectFields.isDefined) {
            workDF.select(transform.selectFields.get.map(col): _*)
        } else {
            workDF
        }
        // rename
        for ((oldColumnName, newColumnName) <- transform.renameFields.getOrElse(Map())) {
            workDF = workDF.withColumnRenamed(oldColumnName, newColumnName)
        }
        // group by
        val groupByColumns  = inputDataConfig.groupByFields.getOrElse(List())
        aggregateInstance(workDF, groupByColumns)
    }

    private def aggregateInstance(dataFrame: DataFrame, groupByColumns: List[String]): DataFrame = {
        val groupedDataFrame = dataFrame.groupBy(groupByColumns.head, groupByColumns.tail: _*)
        val valueColumns = dataFrame.columns.toList filterNot groupByColumns.contains
        val aggToJSON = new AggregateToPayload(valueColumns, groupByColumns).toColumn

        groupedDataFrame.agg(aggToJSON.alias(DFColumn.Instance.toString))
    }

    private def withSchema(dataFrame: DataFrame): DataFrame = {
        val metadataSchema = new StructType(Array(
            StructField("_id", dataType=StringType),
            StructField("availability_zone", dataType=StringType),
            StructField("event_timestamp", dataType=LongType),
            StructField("landb_service_name", dataType=StringType),
            StructField("producer", dataType=StringType),
            StructField("submitter_environment", dataType=StringType),
            StructField("submitter_hostgroup", dataType=StringType),
            StructField("timestamp", dataType=LongType),
            StructField("timestamp_format", dataType=StringType),
            StructField("toplevel_hostgroup", dataType=StringType),
            StructField("type", dataType=StringType),
            StructField("type_prefix", dataType=StringType),
            StructField("version", dataType=StringType),
        ))

        val dataSchema = new StructType(Array(
            StructField("dstype", dataType=StringType),
            StructField("host", dataType=StringType),
            StructField("interval", dataType=DoubleType),
            StructField("plugin", dataType=StringType),
            StructField("plugin_instance", dataType=StringType),
            StructField("time", dataType=DoubleType),
            StructField("type", dataType=StringType),
            StructField("type_instance", dataType=StringType),
            StructField("value", dataType=DoubleType),
        ))

        val schema = new StructType(Array(
            StructField("metadata", dataType=metadataSchema),
            StructField("data", dataType=dataSchema),
        ))

        dataFrame.select(from_json(col(DFColumn.JSONdata.toString).cast("string"), schema)).alias("parsed")
            .select("parsed.*") // .select(col("data.*"), col("metadata"))
    }
}
