package ch.cern.sparkinparallel

import ch.cern.admon.core.data.MonitDataSource
import ch.cern.admon.core.processing.InputConfigProcess
import ch.cern.admon.core.utils.AsyncAwait.await
import ch.cern.sparkinparallel.scenario.TestTask
import org.apache.spark.sql.{DataFrame, SparkSession}

import java.util.concurrent.Executors
import scala.concurrent.{ExecutionContext, Future}

object ParallelJobs {
    def process(spark: SparkSession, kafkaPassword: String, tasks: List[TestTask]): Unit = {

        implicit val jobEC: ExecutionContext = ExecutionContext.fromExecutorService(Executors.newSingleThreadExecutor)
        val executeDTFutures = Future.sequence(tasks.map(task => {
            val ec = ExecutionContext.fromExecutorService(Executors.newSingleThreadExecutor)
            Future {
                val df = processTask(task, spark, kafkaPassword = kafkaPassword)
                df.show(1)
                df.count()
            }(ec)
        }))
        await(executeDTFutures)
    }

    private def processTask(task: TestTask, spark: SparkSession, kafkaPassword: String): DataFrame = {
        val source = new MonitDataSource(spark, task.inputDataConfig.sourceConfigs.head)
        val df = source.readKafka(task.startTask, task.endTask, kafkaPassword)
        InputConfigProcess.processInputConfig(sourceDFs = List(df), inputDataConfig = task.inputDataConfig)
    }
}
