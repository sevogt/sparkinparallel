package ch.cern.sparkinparallel.scenario
import scala.collection.mutable.ListBuffer
import ch.cern.admon.core.model.{InputDataConfig, SourceConfig, SourceFields, TransformConfig}

case class TestTask(detId: Int, inputDataConfig: InputDataConfig, startTask: Long, endTask: Long)

case class TestEntity(id: Int, inputDataConfig: InputDataConfig, intervalMinutes: Int) {
  def getTasks(amount: Int): List[TestTask] = {
    val intervalSeconds: Long = intervalMinutes * 60
    var taskEnd: Long = System.currentTimeMillis() / 1000
    var taskStart: Long = taskEnd - intervalSeconds

    val tasks: ListBuffer[TestTask] = ListBuffer()
    for (_ <- 0 until amount) {
      tasks += TestTask(id, inputDataConfig, taskStart, taskEnd)
      taskEnd = taskStart
      taskStart = taskEnd - intervalSeconds
    }
    tasks.toList
  }
}

object TestScenario {
  private val collectdSource = SourceFields(producer = "collectd", typePrefix = "raw", `type` = "monitoring")
  private val intervalMinutes = 3
  private val taskAmount = 1

  def base: List[TestTask] = {
    generateScenarioTasks(
      id = 0,
      filterExpression = "value == 0",
      selectFields = List("metadata.timestamp", "value", "host", "plugin", "type_instance"),
      renameFields = Map("timestamp" -> "time"),
      groupBy = List("plugin")
    ) ::: generateScenarioTasks(
      id = 1,
      filterExpression = "value > 0",
      selectFields = List("metadata.timestamp", "host", "value", "plugin", "type_instance"),
      renameFields = Map("timestamp" -> "time"),
      groupBy = List("host")
    )
  }

  private def generateScenarioTasks(id: Int,
                                    filterExpression: String,
                                    selectFields: List[String],
                                    renameFields: Map[String, String],
                                    groupBy: List[String]): List[TestTask] = {
    val sc = SourceConfig(
      sourceFields = collectdSource,
      transformConfig = TransformConfig(
        filterExpression = Some(filterExpression),
        selectFields = Some(selectFields),
        renameFields = Some(renameFields)
      )
    )
    TestEntity(
      id = id,
      inputDataConfig = InputDataConfig(
        sourceConfigs = List(sc),
        groupByFields = Some(groupBy)
      ),
      intervalMinutes = intervalMinutes
    ).getTasks(taskAmount)
  }
}
