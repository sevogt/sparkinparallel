package ch.cern.sparkinparallel

import ch.cern.admon.core.data.MonitDataSource
import ch.cern.admon.core.processing.InputConfigProcess
import ch.cern.sparkinparallel.scenario.TestTask
import org.apache.spark.sql.{DataFrame, SparkSession}

object SerialJobs {
    def process(spark: SparkSession, kafkaPassword: String, tasks: List[TestTask]): Unit = {
        for (task <- tasks) {
            val df = processTask(task, spark, kafkaPassword = kafkaPassword)
            df.show(1)
            df.count()
        }
    }

    private def processTask(task: TestTask, spark: SparkSession, kafkaPassword: String): DataFrame = {
        val source = new MonitDataSource(spark, task.inputDataConfig.sourceConfigs.head)
        val df = source.readKafka(task.startTask, task.endTask, kafkaPassword)
        InputConfigProcess.processInputConfig(sourceDFs = List(df), inputDataConfig = task.inputDataConfig)
    }
}
