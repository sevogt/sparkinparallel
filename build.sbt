import sbt.Resolver

organization := "ch.cern"
name := "sparkinparallel"
version := "0.1"


scalaVersion := "2.12.10"
val sparkVersion = "3.2.1"

libraryDependencies ++= Seq(
    "org.apache.spark" %% "spark-sql" % sparkVersion,
    "ch.cern.admon" %% "admon-common" % "0.1"
)

resolvers ++= Seq(
    Resolver.url(
        "ADMON Common",
        url("https://gitlab.cern.ch/admon/admon-common/builds/artifacts/admon112/raw/release/")
    )(Patterns("[orgPath]/[module]/[revision]/[module]-[revision].[ext]?job=build-artifacts"))
)

assemblyMergeStrategy in assembly := {
    case "META-INF/services/org.apache.spark.sql.sources.DataSourceRegister" => MergeStrategy.concat
    case "reference.conf" => MergeStrategy.concat
    case PathList("META-INF", xs@_*) => MergeStrategy.discard
    case x => MergeStrategy.first
}
