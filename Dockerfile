# Modified spark dockerfile from: https://github.com/apache/spark/blob/master/resource-managers/kubernetes/docker/src/main/dockerfiles/spark/Dockerfile
ARG spark_uid=185
ARG java_image_tag=8-jre-slim

FROM openjdk:${java_image_tag}

RUN set -ex && \
    sed -i 's/http:\/\/deb.\(.*\)/https:\/\/deb.\1/g' /etc/apt/sources.list && \
    apt-get update && \
    ln -s /lib /lib64 && \
    apt install -y bash wget tini libc6 libpam-modules krb5-user libnss3 procps && \
    mkdir -p /opt/spark && \
    mkdir -p /opt/spark/jars && \
    mkdir -p /opt/spark/work-dir && \
    touch /opt/spark/RELEASE && \
    rm /bin/sh && \
    ln -sv /bin/bash /bin/sh && \
    echo "auth required pam_wheel.so use_uid" >> /etc/pam.d/su && \
    chgrp root /etc/passwd && chmod ug+rw /etc/passwd && \
    rm -rf /var/cache/apt/*

RUN wget https://downloads.apache.org/spark/spark-3.1.2/spark-3.1.2-bin-hadoop3.2.tgz
RUN tar xvf spark-* && \
    rm spark-*.tgz && \
    mv spark-3.1.2-bin-hadoop3.2/* /opt/spark && \
    cp opt/spark/kubernetes/dockerfiles/spark/decom.sh /opt/decom.sh && \
    cp opt/spark/kubernetes/dockerfiles/spark/entrypoint.sh /opt/entrypoint.sh

ENV SPARK_HOME /opt/spark
WORKDIR /opt/spark/work-dir

COPY resources/krb5.conf /etc/

RUN chmod g+w /opt/spark/work-dir
RUN chmod a+x /opt/spark/kubernetes/dockerfiles/spark/entrypoint.sh
RUN chmod a+x /opt/spark/kubernetes/dockerfiles/spark/decom.sh

COPY target/scala-2.12/sparkinparallel-assembly-0.1.jar /opt/spark/jars/sparkinparallel-assembly-0.1.jar


ENTRYPOINT ["/opt/spark/kubernetes/dockerfiles/spark/entrypoint.sh"]

USER ${spark_uid}
