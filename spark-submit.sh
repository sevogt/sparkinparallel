/usr/local/bin/spark-submit \
    --master k8s://kubernetes.docker.internal:6443 \
    --deploy-mode cluster \
    --name sparkinparallel \
    --class ch.cern.sparkinparallel.Main \
    --conf spark.kubernetes.container.image=docker.io/library/sparkinparallel \
    --conf spark.kubernetes.authenticate.driver.serviceAccountName=spark \
    --conf spark.kubernetes.driver.pod.name=sparkinparallel \
    local:///opt/spark/jars/sparkinparallel-assembly-0.1.jar
